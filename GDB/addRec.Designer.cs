﻿namespace GDB
{
    partial class addRec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupAddRec = new System.Windows.Forms.GroupBox();
            this.tBoxDiscription = new System.Windows.Forms.TextBox();
            this.tBoxPlatform = new System.Windows.Forms.TextBox();
            this.tBoxRate = new System.Windows.Forms.TextBox();
            this.tBoxGenre = new System.Windows.Forms.TextBox();
            this.tBoxPublisher = new System.Windows.Forms.TextBox();
            this.tBoxYear = new System.Windows.Forms.TextBox();
            this.tBoxName = new System.Windows.Forms.TextBox();
            this.labelAddDiscription = new System.Windows.Forms.Label();
            this.labelAddPlatform = new System.Windows.Forms.Label();
            this.labelAddRate = new System.Windows.Forms.Label();
            this.labelAddGenre = new System.Windows.Forms.Label();
            this.labelAddPublisher = new System.Windows.Forms.Label();
            this.labelAddYear = new System.Windows.Forms.Label();
            this.labelAddName = new System.Windows.Forms.Label();
            this.btnAddSubmit = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.picBoxAdd = new System.Windows.Forms.PictureBox();
            this.groupAddRec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // groupAddRec
            // 
            this.groupAddRec.Controls.Add(this.tBoxDiscription);
            this.groupAddRec.Controls.Add(this.tBoxPlatform);
            this.groupAddRec.Controls.Add(this.tBoxRate);
            this.groupAddRec.Controls.Add(this.tBoxGenre);
            this.groupAddRec.Controls.Add(this.tBoxPublisher);
            this.groupAddRec.Controls.Add(this.tBoxYear);
            this.groupAddRec.Controls.Add(this.tBoxName);
            this.groupAddRec.Controls.Add(this.labelAddDiscription);
            this.groupAddRec.Controls.Add(this.labelAddPlatform);
            this.groupAddRec.Controls.Add(this.labelAddRate);
            this.groupAddRec.Controls.Add(this.labelAddGenre);
            this.groupAddRec.Controls.Add(this.labelAddPublisher);
            this.groupAddRec.Controls.Add(this.labelAddYear);
            this.groupAddRec.Controls.Add(this.labelAddName);
            this.groupAddRec.Controls.Add(this.btnAddSubmit);
            this.groupAddRec.Controls.Add(this.btnBrowse);
            this.groupAddRec.Controls.Add(this.picBoxAdd);
            this.groupAddRec.Location = new System.Drawing.Point(12, 12);
            this.groupAddRec.Name = "groupAddRec";
            this.groupAddRec.Size = new System.Drawing.Size(760, 362);
            this.groupAddRec.TabIndex = 0;
            this.groupAddRec.TabStop = false;
            // 
            // tBoxDiscription
            // 
            this.tBoxDiscription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBoxDiscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxDiscription.Location = new System.Drawing.Point(509, 51);
            this.tBoxDiscription.Multiline = true;
            this.tBoxDiscription.Name = "tBoxDiscription";
            this.tBoxDiscription.Size = new System.Drawing.Size(232, 206);
            this.tBoxDiscription.TabIndex = 7;
            // 
            // tBoxPlatform
            // 
            this.tBoxPlatform.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxPlatform.Location = new System.Drawing.Point(321, 258);
            this.tBoxPlatform.Name = "tBoxPlatform";
            this.tBoxPlatform.Size = new System.Drawing.Size(150, 27);
            this.tBoxPlatform.TabIndex = 6;
            // 
            // tBoxRate
            // 
            this.tBoxRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxRate.Location = new System.Drawing.Point(321, 210);
            this.tBoxRate.Name = "tBoxRate";
            this.tBoxRate.Size = new System.Drawing.Size(150, 27);
            this.tBoxRate.TabIndex = 5;
            // 
            // tBoxGenre
            // 
            this.tBoxGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxGenre.Location = new System.Drawing.Point(321, 162);
            this.tBoxGenre.Name = "tBoxGenre";
            this.tBoxGenre.Size = new System.Drawing.Size(150, 27);
            this.tBoxGenre.TabIndex = 4;
            // 
            // tBoxPublisher
            // 
            this.tBoxPublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxPublisher.Location = new System.Drawing.Point(321, 114);
            this.tBoxPublisher.Name = "tBoxPublisher";
            this.tBoxPublisher.Size = new System.Drawing.Size(150, 27);
            this.tBoxPublisher.TabIndex = 3;
            // 
            // tBoxYear
            // 
            this.tBoxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxYear.Location = new System.Drawing.Point(321, 67);
            this.tBoxYear.Name = "tBoxYear";
            this.tBoxYear.Size = new System.Drawing.Size(150, 27);
            this.tBoxYear.TabIndex = 2;
            // 
            // tBoxName
            // 
            this.tBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxName.Location = new System.Drawing.Point(321, 19);
            this.tBoxName.Name = "tBoxName";
            this.tBoxName.Size = new System.Drawing.Size(150, 27);
            this.tBoxName.TabIndex = 1;
            // 
            // labelAddDiscription
            // 
            this.labelAddDiscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddDiscription.Location = new System.Drawing.Point(583, 21);
            this.labelAddDiscription.Name = "labelAddDiscription";
            this.labelAddDiscription.Size = new System.Drawing.Size(89, 27);
            this.labelAddDiscription.TabIndex = 2;
            this.labelAddDiscription.Text = "Discription";
            // 
            // labelAddPlatform
            // 
            this.labelAddPlatform.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddPlatform.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddPlatform.Location = new System.Drawing.Point(222, 258);
            this.labelAddPlatform.Name = "labelAddPlatform";
            this.labelAddPlatform.Size = new System.Drawing.Size(93, 27);
            this.labelAddPlatform.TabIndex = 2;
            this.labelAddPlatform.Text = "Platform";
            // 
            // labelAddRate
            // 
            this.labelAddRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddRate.Location = new System.Drawing.Point(222, 210);
            this.labelAddRate.Name = "labelAddRate";
            this.labelAddRate.Size = new System.Drawing.Size(93, 27);
            this.labelAddRate.TabIndex = 2;
            this.labelAddRate.Text = "Rate";
            // 
            // labelAddGenre
            // 
            this.labelAddGenre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddGenre.Location = new System.Drawing.Point(222, 162);
            this.labelAddGenre.Name = "labelAddGenre";
            this.labelAddGenre.Size = new System.Drawing.Size(93, 27);
            this.labelAddGenre.TabIndex = 2;
            this.labelAddGenre.Text = "Genre";
            // 
            // labelAddPublisher
            // 
            this.labelAddPublisher.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddPublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddPublisher.Location = new System.Drawing.Point(222, 114);
            this.labelAddPublisher.Name = "labelAddPublisher";
            this.labelAddPublisher.Size = new System.Drawing.Size(93, 27);
            this.labelAddPublisher.TabIndex = 2;
            this.labelAddPublisher.Text = "Publisher";
            // 
            // labelAddYear
            // 
            this.labelAddYear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddYear.Location = new System.Drawing.Point(222, 67);
            this.labelAddYear.Name = "labelAddYear";
            this.labelAddYear.Size = new System.Drawing.Size(93, 27);
            this.labelAddYear.TabIndex = 2;
            this.labelAddYear.Text = "Year";
            // 
            // labelAddName
            // 
            this.labelAddName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelAddName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddName.Location = new System.Drawing.Point(222, 19);
            this.labelAddName.Name = "labelAddName";
            this.labelAddName.Size = new System.Drawing.Size(93, 27);
            this.labelAddName.TabIndex = 2;
            this.labelAddName.Text = "Name";
            // 
            // btnAddSubmit
            // 
            this.btnAddSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubmit.Location = new System.Drawing.Point(548, 274);
            this.btnAddSubmit.Name = "btnAddSubmit";
            this.btnAddSubmit.Size = new System.Drawing.Size(159, 41);
            this.btnAddSubmit.TabIndex = 9;
            this.btnAddSubmit.Text = "Submit";
            this.btnAddSubmit.UseVisualStyleBackColor = true;
            this.btnAddSubmit.Click += new System.EventHandler(this.btnAddSubmit_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(30, 301);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(159, 41);
            this.btnBrowse.TabIndex = 8;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // picBoxAdd
            // 
            this.picBoxAdd.Location = new System.Drawing.Point(6, 19);
            this.picBoxAdd.Name = "picBoxAdd";
            this.picBoxAdd.Size = new System.Drawing.Size(210, 276);
            this.picBoxAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxAdd.TabIndex = 0;
            this.picBoxAdd.TabStop = false;
            // 
            // addRec
            // 
            this.AcceptButton = this.btnAddSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 386);
            this.Controls.Add(this.groupAddRec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addRec";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Add Record";
            this.groupAddRec.ResumeLayout(false);
            this.groupAddRec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAdd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupAddRec;
        private System.Windows.Forms.Label labelAddDiscription;
        private System.Windows.Forms.Label labelAddPlatform;
        private System.Windows.Forms.Label labelAddRate;
        private System.Windows.Forms.Label labelAddGenre;
        private System.Windows.Forms.Label labelAddPublisher;
        private System.Windows.Forms.Label labelAddYear;
        private System.Windows.Forms.Label labelAddName;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox picBoxAdd;
        private System.Windows.Forms.TextBox tBoxDiscription;
        private System.Windows.Forms.TextBox tBoxPlatform;
        private System.Windows.Forms.TextBox tBoxRate;
        private System.Windows.Forms.TextBox tBoxGenre;
        private System.Windows.Forms.TextBox tBoxPublisher;
        private System.Windows.Forms.TextBox tBoxYear;
        private System.Windows.Forms.TextBox tBoxName;
        private System.Windows.Forms.Button btnAddSubmit;
    }
}