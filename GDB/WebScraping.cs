﻿using System;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDB
{
    public class WebScraping
    {
        private HtmlWeb web = new HtmlWeb();
        private HtmlDocument doc;
        public string getWebContent(string url, string xPath)
        {
            string result = "";
            doc = web.Load(url);
            try
            {
                result = doc.DocumentNode.SelectNodes(xPath)[0].InnerText;
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException("Could not find the content on the web.");
            }
            return result;
        }
    }
}
