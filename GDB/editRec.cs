﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

/* 
 * Alireza Meftahi
*/

namespace GDB
{
    public partial class editRec : Form
    {
        public editRec()
        {
            InitializeComponent();
        }
        public bool haspic = false;
        public int _idOfGame;

        private void btnEditSubmit_Click(object sender, EventArgs e)
        {
            Game tempGame = new Game();
            tempGame = Dic.recs[Dic.candidateGame];
            XmlDocument doc = new XmlDocument();
            doc.Load("GDB.xml");
            XmlNode rootNode = doc.SelectSingleNode("root");
            XmlNode gameNode = rootNode.SelectSingleNode("game" + tempGame.Id.ToString());
            XmlNode idNode = gameNode.SelectSingleNode("id");
            XmlNode nameNode = gameNode.SelectSingleNode("name");
            XmlNode yearNode = gameNode.SelectSingleNode("year");
            XmlNode publisherNode = gameNode.SelectSingleNode("publisher");
            XmlNode genreNode = gameNode.SelectSingleNode("genre");
            XmlNode rateNode = gameNode.SelectSingleNode("rate");
            XmlNode platformNode = gameNode.SelectSingleNode("platform");
            XmlNode discriptionNode = gameNode.SelectSingleNode("discription");
            nameNode.InnerText = tBoxNameEdit.Text;
            yearNode.InnerText = tBoxYearEdit.Text;
            publisherNode.InnerText = tBoxPublisherEdit.Text;
            genreNode.InnerText = tBoxGenreEdit.Text;
            rateNode.InnerText = tBoxRateEdit.Text;
            platformNode.InnerText = tBoxPlatformEdit.Text;
            discriptionNode.InnerText = tBoxDiscriptionEdit.Text;
            doc.Save("GDB.xml");
            if (haspic)
            {
                Image img = picBoxEdit.Image;
                img.Save("./imgs/image" + idNode.InnerText + ".ai");
                img.Dispose();
            }
            haspic = false;
            Close();
            Dic.recs.Clear();
            mainForm.loadData();
            mainForm.refreshListBox();

        }
        private void editRec_Load(object sender, EventArgs e)
        {
            Game tempGame = new Game();
            tempGame = Dic.recs[Dic.candidateGame];
            XmlDocument doc = new XmlDocument();
            doc.Load("GDB.xml");
            XmlNode rootNode = doc.SelectSingleNode("root");
            XmlNode gameNode = rootNode.SelectSingleNode("game" + tempGame.Id.ToString());
            XmlNode idNode = gameNode.SelectSingleNode("id");
            XmlNode nameNode = gameNode.SelectSingleNode("name");
            XmlNode yearNode = gameNode.SelectSingleNode("year");
            XmlNode publisherNode = gameNode.SelectSingleNode("publisher");
            XmlNode genreNode = gameNode.SelectSingleNode("genre");
            XmlNode rateNode = gameNode.SelectSingleNode("rate");
            XmlNode platformNode = gameNode.SelectSingleNode("platform");
            XmlNode discriptionNode = gameNode.SelectSingleNode("discription");
            _idOfGame = Convert.ToInt32(idNode.InnerText);
            tBoxNameEdit.Text = nameNode.InnerText;
            tBoxYearEdit.Text = yearNode.InnerText;
            tBoxPublisherEdit.Text = publisherNode.InnerText;
            tBoxGenreEdit.Text = genreNode.InnerText;
            tBoxRateEdit.Text = rateNode.InnerText;
            tBoxPlatformEdit.Text = platformNode.InnerText;
            tBoxDiscriptionEdit.Text = discriptionNode.InnerText;
            if (File.Exists("./imgs/image" + idNode.InnerText + ".ai"))
            {
                Image img = new Bitmap("./imgs/image" + idNode.InnerText + ".ai");
                picBoxEdit.Image = img.GetThumbnailImage(210, 276, null, new IntPtr());
                img.Dispose();
            }
        }

        private void btnBrowseEdit_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                Image img = new Bitmap(open.FileName);
                picBoxEdit.Image = img.GetThumbnailImage(210, 276, null, new IntPtr());
                open.RestoreDirectory = true;
                haspic = true;
                img.Dispose();
            }
        }

        private void btnRemovePic_Click(object sender, EventArgs e)
        {
            if (File.Exists("./imgs/image" + _idOfGame.ToString() + ".ai"))
            {
                try
                {
                    File.Delete("./imgs/image" + _idOfGame.ToString() + ".ai");
                }
                catch (IOException err)
                {
                    MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            picBoxEdit.Image = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebScraping webScrapper = new WebScraping();
            string name = tBoxNameEdit.Text;
            string url = "https://en.wikipedia.org/wiki/" + Uri.EscapeUriString(name);
            string xPath = "/html/body/div[3]/div[3]/div[4]/div/p[1]";
            string res = "";
            try
            {
                res = webScrapper.getWebContent(url, xPath);
            }
            catch (NullReferenceException err)
            {
                MessageBox.Show(err.Message);
            }
            tBoxDiscriptionEdit.Text = res;
        }
    }
}
