﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDB
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Publisher { get; set; }
        public string Genre { get; set; }
        public string Rate { get; set; }
        public string Platform { get; set; }
        public string Discription { get; set; }

    }
}
