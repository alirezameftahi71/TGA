﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Game Data Base")]
[assembly: AssemblyDescription("A Game Data Base")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("A.R.M.")]
[assembly: AssemblyProduct("GDB")]
[assembly: AssemblyCopyright("Copyright © A.R.M. 2017")]
[assembly: AssemblyTrademark("A.R.M.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("71f584f7-286e-49f8-b3bb-507f09920955")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.1.0.0")]
[assembly: AssemblyFileVersion("7.1.0.0")]
[assembly: NeutralResourcesLanguage("en-US")]

