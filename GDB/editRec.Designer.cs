﻿namespace GDB
{
    partial class editRec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupEditRec = new System.Windows.Forms.GroupBox();
            this.tBoxDiscriptionEdit = new System.Windows.Forms.TextBox();
            this.tBoxPlatformEdit = new System.Windows.Forms.TextBox();
            this.tBoxRateEdit = new System.Windows.Forms.TextBox();
            this.tBoxGenreEdit = new System.Windows.Forms.TextBox();
            this.tBoxPublisherEdit = new System.Windows.Forms.TextBox();
            this.tBoxYearEdit = new System.Windows.Forms.TextBox();
            this.tBoxNameEdit = new System.Windows.Forms.TextBox();
            this.labelAddDiscription = new System.Windows.Forms.Label();
            this.labelEditPlatform = new System.Windows.Forms.Label();
            this.labelEditRate = new System.Windows.Forms.Label();
            this.labelEditGenre = new System.Windows.Forms.Label();
            this.labelEditPublisher = new System.Windows.Forms.Label();
            this.labelEditYear = new System.Windows.Forms.Label();
            this.labelEditName = new System.Windows.Forms.Label();
            this.btnEditSubmit = new System.Windows.Forms.Button();
            this.btnRemovePic = new System.Windows.Forms.Button();
            this.btnBrowseEdit = new System.Windows.Forms.Button();
            this.picBoxEdit = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupEditRec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // groupEditRec
            // 
            this.groupEditRec.Controls.Add(this.button1);
            this.groupEditRec.Controls.Add(this.tBoxDiscriptionEdit);
            this.groupEditRec.Controls.Add(this.tBoxPlatformEdit);
            this.groupEditRec.Controls.Add(this.tBoxRateEdit);
            this.groupEditRec.Controls.Add(this.tBoxGenreEdit);
            this.groupEditRec.Controls.Add(this.tBoxPublisherEdit);
            this.groupEditRec.Controls.Add(this.tBoxYearEdit);
            this.groupEditRec.Controls.Add(this.tBoxNameEdit);
            this.groupEditRec.Controls.Add(this.labelAddDiscription);
            this.groupEditRec.Controls.Add(this.labelEditPlatform);
            this.groupEditRec.Controls.Add(this.labelEditRate);
            this.groupEditRec.Controls.Add(this.labelEditGenre);
            this.groupEditRec.Controls.Add(this.labelEditPublisher);
            this.groupEditRec.Controls.Add(this.labelEditYear);
            this.groupEditRec.Controls.Add(this.labelEditName);
            this.groupEditRec.Controls.Add(this.btnEditSubmit);
            this.groupEditRec.Controls.Add(this.btnRemovePic);
            this.groupEditRec.Controls.Add(this.btnBrowseEdit);
            this.groupEditRec.Controls.Add(this.picBoxEdit);
            this.groupEditRec.Location = new System.Drawing.Point(32, 29);
            this.groupEditRec.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupEditRec.Name = "groupEditRec";
            this.groupEditRec.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupEditRec.Size = new System.Drawing.Size(2027, 863);
            this.groupEditRec.TabIndex = 1;
            this.groupEditRec.TabStop = false;
            // 
            // tBoxDiscriptionEdit
            // 
            this.tBoxDiscriptionEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBoxDiscriptionEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxDiscriptionEdit.Location = new System.Drawing.Point(1357, 122);
            this.tBoxDiscriptionEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxDiscriptionEdit.Multiline = true;
            this.tBoxDiscriptionEdit.Name = "tBoxDiscriptionEdit";
            this.tBoxDiscriptionEdit.Size = new System.Drawing.Size(619, 491);
            this.tBoxDiscriptionEdit.TabIndex = 7;
            // 
            // tBoxPlatformEdit
            // 
            this.tBoxPlatformEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxPlatformEdit.Location = new System.Drawing.Point(856, 615);
            this.tBoxPlatformEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxPlatformEdit.Name = "tBoxPlatformEdit";
            this.tBoxPlatformEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxPlatformEdit.TabIndex = 6;
            // 
            // tBoxRateEdit
            // 
            this.tBoxRateEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxRateEdit.Location = new System.Drawing.Point(856, 501);
            this.tBoxRateEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxRateEdit.Name = "tBoxRateEdit";
            this.tBoxRateEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxRateEdit.TabIndex = 5;
            // 
            // tBoxGenreEdit
            // 
            this.tBoxGenreEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxGenreEdit.Location = new System.Drawing.Point(856, 386);
            this.tBoxGenreEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxGenreEdit.Name = "tBoxGenreEdit";
            this.tBoxGenreEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxGenreEdit.TabIndex = 4;
            // 
            // tBoxPublisherEdit
            // 
            this.tBoxPublisherEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxPublisherEdit.Location = new System.Drawing.Point(856, 272);
            this.tBoxPublisherEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxPublisherEdit.Name = "tBoxPublisherEdit";
            this.tBoxPublisherEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxPublisherEdit.TabIndex = 3;
            // 
            // tBoxYearEdit
            // 
            this.tBoxYearEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxYearEdit.Location = new System.Drawing.Point(856, 160);
            this.tBoxYearEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxYearEdit.Name = "tBoxYearEdit";
            this.tBoxYearEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxYearEdit.TabIndex = 2;
            // 
            // tBoxNameEdit
            // 
            this.tBoxNameEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxNameEdit.Location = new System.Drawing.Point(856, 45);
            this.tBoxNameEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tBoxNameEdit.Name = "tBoxNameEdit";
            this.tBoxNameEdit.Size = new System.Drawing.Size(393, 57);
            this.tBoxNameEdit.TabIndex = 1;
            // 
            // labelAddDiscription
            // 
            this.labelAddDiscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddDiscription.Location = new System.Drawing.Point(1555, 50);
            this.labelAddDiscription.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelAddDiscription.Name = "labelAddDiscription";
            this.labelAddDiscription.Size = new System.Drawing.Size(237, 64);
            this.labelAddDiscription.TabIndex = 2;
            this.labelAddDiscription.Text = "Discription";
            // 
            // labelEditPlatform
            // 
            this.labelEditPlatform.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditPlatform.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditPlatform.Location = new System.Drawing.Point(592, 615);
            this.labelEditPlatform.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditPlatform.Name = "labelEditPlatform";
            this.labelEditPlatform.Size = new System.Drawing.Size(248, 64);
            this.labelEditPlatform.TabIndex = 2;
            this.labelEditPlatform.Text = "Platform";
            // 
            // labelEditRate
            // 
            this.labelEditRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditRate.Location = new System.Drawing.Point(592, 501);
            this.labelEditRate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditRate.Name = "labelEditRate";
            this.labelEditRate.Size = new System.Drawing.Size(248, 64);
            this.labelEditRate.TabIndex = 2;
            this.labelEditRate.Text = "Rate";
            // 
            // labelEditGenre
            // 
            this.labelEditGenre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditGenre.Location = new System.Drawing.Point(592, 386);
            this.labelEditGenre.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditGenre.Name = "labelEditGenre";
            this.labelEditGenre.Size = new System.Drawing.Size(248, 64);
            this.labelEditGenre.TabIndex = 2;
            this.labelEditGenre.Text = "Genre";
            // 
            // labelEditPublisher
            // 
            this.labelEditPublisher.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditPublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditPublisher.Location = new System.Drawing.Point(592, 272);
            this.labelEditPublisher.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditPublisher.Name = "labelEditPublisher";
            this.labelEditPublisher.Size = new System.Drawing.Size(248, 64);
            this.labelEditPublisher.TabIndex = 2;
            this.labelEditPublisher.Text = "Publisher";
            // 
            // labelEditYear
            // 
            this.labelEditYear.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditYear.Location = new System.Drawing.Point(592, 160);
            this.labelEditYear.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditYear.Name = "labelEditYear";
            this.labelEditYear.Size = new System.Drawing.Size(248, 64);
            this.labelEditYear.TabIndex = 2;
            this.labelEditYear.Text = "Year";
            // 
            // labelEditName
            // 
            this.labelEditName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelEditName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEditName.Location = new System.Drawing.Point(592, 45);
            this.labelEditName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelEditName.Name = "labelEditName";
            this.labelEditName.Size = new System.Drawing.Size(248, 64);
            this.labelEditName.TabIndex = 2;
            this.labelEditName.Text = "Name";
            // 
            // btnEditSubmit
            // 
            this.btnEditSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditSubmit.Location = new System.Drawing.Point(1461, 653);
            this.btnEditSubmit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnEditSubmit.Name = "btnEditSubmit";
            this.btnEditSubmit.Size = new System.Drawing.Size(424, 98);
            this.btnEditSubmit.TabIndex = 9;
            this.btnEditSubmit.Text = "Submit";
            this.btnEditSubmit.UseVisualStyleBackColor = true;
            this.btnEditSubmit.Click += new System.EventHandler(this.btnEditSubmit_Click);
            // 
            // btnRemovePic
            // 
            this.btnRemovePic.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemovePic.Location = new System.Drawing.Point(16, 718);
            this.btnRemovePic.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnRemovePic.Name = "btnRemovePic";
            this.btnRemovePic.Size = new System.Drawing.Size(341, 98);
            this.btnRemovePic.TabIndex = 8;
            this.btnRemovePic.Text = "Remove Picture";
            this.btnRemovePic.UseVisualStyleBackColor = true;
            this.btnRemovePic.Click += new System.EventHandler(this.btnRemovePic_Click);
            // 
            // btnBrowseEdit
            // 
            this.btnBrowseEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseEdit.Location = new System.Drawing.Point(373, 718);
            this.btnBrowseEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnBrowseEdit.Name = "btnBrowseEdit";
            this.btnBrowseEdit.Size = new System.Drawing.Size(203, 98);
            this.btnBrowseEdit.TabIndex = 8;
            this.btnBrowseEdit.Text = "Browse";
            this.btnBrowseEdit.UseVisualStyleBackColor = true;
            this.btnBrowseEdit.Click += new System.EventHandler(this.btnBrowseEdit_Click);
            // 
            // picBoxEdit
            // 
            this.picBoxEdit.Location = new System.Drawing.Point(16, 45);
            this.picBoxEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.picBoxEdit.Name = "picBoxEdit";
            this.picBoxEdit.Size = new System.Drawing.Size(560, 658);
            this.picBoxEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxEdit.TabIndex = 0;
            this.picBoxEdit.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(592, 718);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 98);
            this.button1.TabIndex = 10;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // editRec
            // 
            this.AcceptButton = this.btnEditSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2091, 920);
            this.Controls.Add(this.groupEditRec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "editRec";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Update Record";
            this.Load += new System.EventHandler(this.editRec_Load);
            this.groupEditRec.ResumeLayout(false);
            this.groupEditRec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupEditRec;
        private System.Windows.Forms.TextBox tBoxDiscriptionEdit;
        private System.Windows.Forms.TextBox tBoxPlatformEdit;
        private System.Windows.Forms.TextBox tBoxRateEdit;
        private System.Windows.Forms.TextBox tBoxGenreEdit;
        private System.Windows.Forms.TextBox tBoxPublisherEdit;
        private System.Windows.Forms.TextBox tBoxYearEdit;
        private System.Windows.Forms.TextBox tBoxNameEdit;
        private System.Windows.Forms.Label labelAddDiscription;
        private System.Windows.Forms.Label labelEditPlatform;
        private System.Windows.Forms.Label labelEditRate;
        private System.Windows.Forms.Label labelEditGenre;
        private System.Windows.Forms.Label labelEditPublisher;
        private System.Windows.Forms.Label labelEditYear;
        private System.Windows.Forms.Label labelEditName;
        private System.Windows.Forms.Button btnEditSubmit;
        private System.Windows.Forms.Button btnBrowseEdit;
        private System.Windows.Forms.PictureBox picBoxEdit;
        private System.Windows.Forms.Button btnRemovePic;
        private System.Windows.Forms.Button button1;
    }
}