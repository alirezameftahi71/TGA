﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

/* 
 * Alireza Meftahi
*/

namespace GDB
{
    public partial class addRec : Form
    {
        public bool haspic = false;
        public int _autoNum;
        string _sendName;
        string _gameName;
        public addRec()
        {
            InitializeComponent();
        }

        private void btnAddSubmit_Click(object sender, EventArgs e)
        {
            _autoNum = Properties.Settings.Default.trackNums;
            
            try
            {
                if (tBoxName.Text != "")
                {
                    if (File.Exists("GDB.xml"))
                    {
                        Game game = new Game();
                        game.Id = _autoNum;
                        game.Name = tBoxName.Text;
                        _gameName = game.Name;
                        _sendName = tBoxName.Text;
                        game.Year = tBoxYear.Text;
                        game.Publisher = tBoxPublisher.Text;
                        game.Genre = tBoxGenre.Text;
                        game.Rate = tBoxRate.Text;
                        game.Platform = tBoxPlatform.Text;
                        game.Discription = tBoxDiscription.Text;
                        Dic.recs.Add(game.Name, game);
                        XDocument doc = XDocument.Load("GDB.xml");
                        XElement gameRoot = new XElement("game" + game.Id.ToString());
                        gameRoot.Add(
                            new XElement("id", game.Id.ToString()),
                            new XElement("name", game.Name),
                            new XElement("year", game.Year.ToString()),
                            new XElement("publisher", game.Publisher),
                            new XElement("genre", game.Genre),
                            new XElement("rate", game.Rate.ToString()),
                            new XElement("platform", game.Platform),
                            new XElement("discription", game.Discription)
                        );
                        doc.Element("root").Add(gameRoot);
                        doc.Save("GDB.xml");
                    }
                    else
                    {
                        Properties.Settings.Default.trackNums = 1;
                        Properties.Settings.Default.Save();
                        mainForm.setIndexFile();
                        _autoNum = Properties.Settings.Default.trackNums;
                        Game game = new Game();
                        game.Id = _autoNum;
                        game.Name = tBoxName.Text;
                        _gameName = game.Name;
                        game.Year = tBoxYear.Text;
                        game.Publisher = tBoxPublisher.Text;
                        game.Genre = tBoxGenre.Text;
                        game.Rate = tBoxRate.Text;
                        game.Platform = tBoxPlatform.Text;
                        game.Discription = tBoxDiscription.Text;
                        Dic.recs.Add(game.Name, game);
                        new XDocument(
                            new XElement("root",
                                new XElement("game" + game.Id.ToString(),
                                    new XElement("id", game.Id.ToString()),
                                new XElement("name", game.Name),
                                new XElement("year", game.Year.ToString()),
                                new XElement("publisher", game.Publisher),
                                new XElement("genre", game.Genre),
                                new XElement("rate", game.Rate.ToString()),
                                new XElement("platform", game.Platform),
                                new XElement("discription", game.Discription)
                                ))).Save("GDB.xml");
                    }
                    MessageBox.Show("Successfully Saved", "Done", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    if (haspic)
                    {
                        Image img = picBoxAdd.Image;
                        img.Save("./imgs/image" + _autoNum.ToString() +".ai");
                        img.Dispose();
                    }
                    Properties.Settings.Default.trackNums = _autoNum + 1;
                    Properties.Settings.Default.Save();
                    mainForm.setIndexFile();
                    tBoxName.Text = "";
                    tBoxGenre.Text = "";
                    tBoxDiscription.Text = "";
                    tBoxPlatform.Text = "";
                    tBoxPublisher.Text = "";
                    tBoxRate.Text = "";
                    tBoxYear.Text = "";
                    picBoxAdd.Image = null;
                    tBoxName.Focus();
                    haspic = false;
                }
                else
                {
                    MessageBox.Show("You have to input a name at least", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                mainForm.refreshListBox();
                mainForm.refreshListBox(mainForm.whichIndexList(_sendName));
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Save.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                Image img = new Bitmap(open.FileName);
                picBoxAdd.Image = img.GetThumbnailImage(210, 276, null, new IntPtr());
                open.RestoreDirectory = true;
                haspic = true;
                img.Dispose();
            }
        }
    }
}
