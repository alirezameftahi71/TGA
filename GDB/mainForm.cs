﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Threading.Tasks;

/* 
 * Author : Ali Reza Meftahi
 * Started : Aug 17 '16
 * Latest Update : Feb 18 '17
*/

namespace GDB
{
    public partial class mainForm : Form
    {
        private static mainForm _instance;
        public mainForm()
        {
            InitializeComponent();
            _instance = this;
            Disposed += UserControl1_Disposed;
            if (Properties.Settings.Default.isSorted)
            {
                SortList(true);
            }
        }
        public static void upStat()
        {
            if (Properties.Settings.Default.isStatusEnabled)
            {
                _instance.statusStrip1.Visible = true;
                _instance.toolStripStatusLabel1.Text = "Total records : " + _instance.listBox1.Items.Count.ToString() + "    ";
                if (newList.recs.ContainsKey(_instance.listBox1.GetItemText(_instance.listBox1.SelectedItem)))
                {
                    _instance.toolStripStatusLabel2.Image = Properties.Resources.hearts;
                }
                else
                {
                    _instance.toolStripStatusLabel2.Image = null;
                }
            }
            else
            {
                _instance.statusStrip1.Visible = false;
            }
        }
        public static void upStat(Game newGame)
        {
            _instance.toolStripStatusLabel3.Text = "    " + newGame.Id.ToString() + " - " + _instance.lbShowName.Text + "    ";
            if (newList.recs.ContainsKey(_instance.listBox1.GetItemText(_instance.listBox1.SelectedItem)))
            {
                _instance.toolStripStatusLabel2.Image = Properties.Resources.hearts;
            }
            else
            {
                _instance.toolStripStatusLabel2.Image = null;
            }
        }
        private void UserControl1_Disposed(object sender, EventArgs e)
        {
            if (toolTip1 != null)
                toolTip1.Dispose();
        }
        private void htmlMaker()
        {
            string htmlContent = "<html><head><title>The Print Preview</title><headtitle>Total number of records: " + _instance.listBox1.Items.Count.ToString() +
                "</headtitle><link href='reportStyle.css' rel='stylesheet' /></head><body>";
            foreach (var item in Dic.recs)
            {
                htmlContent += "<div><p class='id'>" + item.Value.Id + "</p>" +
                    "<p class='name'><b>Title: </b>" + item.Value.Name + "</p>" +
                    "<p class='year'><b>Released Year: </b>" + item.Value.Year + "</p>" +
                    "<p class='publisher'><b>Publisher(s): </b>" + item.Value.Publisher + "</p>" +
                    "<p class='genre'><b>Gnere(s): </b>" + item.Value.Genre + "</p>" +
                    "<p class='platform'><b>Platform(s): </b>" + item.Value.Platform + "</p>" +
                    "<p class='description'><b>Description: </b>" + item.Value.Discription + "</p>" +
                    "</div>";
            }
            htmlContent += "</body><script src='script.js'></script></html>";
            File.WriteAllText(@".\htmlGDB.html", htmlContent);
        }
        public static int whichIndexList(string nm)
        {
            try
            {
                return _instance.listBox1.Items.IndexOf(nm);
            }
            catch (Exception)
            {
                refreshListBox();
                return 0;
            }
        }
        public static void loadData()
        {
            try
            {
                if (File.Exists("GDB.xml"))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load("GDB.xml");
                    XmlNode rootNode = doc.SelectSingleNode("root");
                    foreach (XmlNode gameNode in rootNode.ChildNodes)
                    {
                        Game loadGame = new Game();
                        XmlNode idNode = gameNode.SelectSingleNode("id");
                        XmlNode nameNode = gameNode.SelectSingleNode("name");
                        XmlNode yearNode = gameNode.SelectSingleNode("year");
                        XmlNode publisherNode = gameNode.SelectSingleNode("publisher");
                        XmlNode genreNode = gameNode.SelectSingleNode("genre");
                        XmlNode rateNode = gameNode.SelectSingleNode("rate");
                        XmlNode platformNode = gameNode.SelectSingleNode("platform");
                        XmlNode disNode = gameNode.SelectSingleNode("discription");
                        loadGame.Id = Convert.ToInt32(idNode.InnerText);
                        loadGame.Name = nameNode.InnerText;
                        loadGame.Year = yearNode.InnerText;
                        loadGame.Publisher = publisherNode.InnerText;
                        loadGame.Genre = genreNode.InnerText;
                        loadGame.Rate = rateNode.InnerText;
                        loadGame.Platform = platformNode.InnerText;
                        loadGame.Discription = disNode.InnerText;
                        Dic.recs.Add(loadGame.Name, loadGame);
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static void refreshListBox()
        {
            try
            {
                _instance.listBox1.DataSource = null;
                if (File.Exists("GDB.xml"))
                {
                    int x = _instance.listBox1.SelectedIndex;
                    _instance.listBox1.Items.Clear();
                    _instance.lbShowGenre.Text = "-";
                    _instance.lbShowName.Text = "-";
                    _instance.lbShowPlatform.Text = "-";
                    _instance.lbShowPublisher.Text = "-";
                    _instance.lbShowRate.Text = "-";
                    _instance.lbShowYear.Text = "-";
                    _instance.labelDiscription.Text = "-";
                    _instance.picBoxMain.Image = null;
                    XmlDocument doc = new XmlDocument();
                    doc.Load("GDB.xml");
                    XmlNode node0 = doc.SelectSingleNode("root");
                    foreach (XmlNode node1 in node0.ChildNodes)
                    {
                        XmlNode node2 = node1.SelectSingleNode("name");
                        _instance.listBox1.Items.Add(node2.InnerText);
                    }
                    _instance.listBox1.SelectedIndex = x;
                }
                upStat();
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static void refreshListBox(int x)
        {
            try
            {
                _instance.listBox1.DataSource = null;
                _instance.searchBox1.ResetText();
                if (File.Exists("GDB.xml"))
                {
                    _instance.listBox1.Items.Clear();
                    _instance.lbShowGenre.Text = "-";
                    _instance.lbShowName.Text = "-";
                    _instance.lbShowPlatform.Text = "-";
                    _instance.lbShowPublisher.Text = "-";
                    _instance.lbShowRate.Text = "-";
                    _instance.lbShowYear.Text = "-";
                    _instance.labelDiscription.Text = "-";
                    _instance.picBoxMain.Image = null;
                    XmlDocument doc = new XmlDocument();
                    doc.Load("GDB.xml");
                    XmlNode node0 = doc.SelectSingleNode("root");
                    foreach (XmlNode node1 in node0.ChildNodes)
                    {
                        XmlNode node2 = node1.SelectSingleNode("name");
                        _instance.listBox1.Items.Add(node2.InnerText);
                    }
                    _instance.listBox1.SelectedIndex = x;
                }
                upStat();
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteRec()
        {
            if (File.Exists("GDB.xml"))
            {
                try
                {
                    var i = MessageBox.Show("Are you sure you want to Delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (i.ToString() == "No")
                    {
                        return;
                    }
                    int x = listBox1.SelectedIndex;
                    listForm.deleteRec(listBox1.GetItemText(listBox1.SelectedItem));
                    Game delGame = new Game();
                    delGame = Dic.recs[listBox1.GetItemText(listBox1.SelectedItem)];
                    XmlDocument doc = new XmlDocument();
                    doc.Load("GDB.xml");
                    XmlNode rootNode = doc.SelectSingleNode("root");
                    XmlNode gameNode = rootNode.SelectSingleNode("game" + delGame.Id.ToString());
                    rootNode.RemoveChild(gameNode);
                    doc.Save("GDB.xml");
                    Dic.recs.Remove(listBox1.GetItemText(listBox1.SelectedItem));
                    if (File.Exists("./imgs/image" + delGame.Id.ToString() + ".ai"))
                    {
                        File.Delete("./imgs/image" + delGame.Id.ToString() + ".ai");
                    }
                    picBoxMain.Image = null;
                    refreshListBox(x - 1);
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to Remove", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No Data Base!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void updateRec()
        {
            if (File.Exists("GDB.xml"))
            {
                try
                {
                    Game edtGame = new Game();
                    edtGame = Dic.recs[lbShowName.Text];
                    Dic.candidateGame = edtGame.Name;
                    editRec recEdit = new editRec();
                    recEdit.ShowDialog();
                }
                catch (Exception)
                {
                    MessageBox.Show("No selection", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("No Data Base!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void resetDataBase()
        {
            var i = MessageBox.Show("Are you sure you want to RESET the Data Base?", "RESET", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (i.ToString() == "No")
            {
                return;
            }
            try
            {
                File.Delete("GDB.xml");
                File.Delete("reportStyle.css");
                Directory.Delete("./imgs", true);
                Properties.Settings.Default.trackNums = 1;
                Properties.Settings.Default.Save();
                setIndexFile();
                Dic.recs.Clear();
                listBox1.Items.Clear();
                Directory.CreateDirectory("./imgs");
                lbShowGenre.Text = "-";
                lbShowName.Text = "-";
                lbShowPlatform.Text = "-";
                lbShowPublisher.Text = "-";
                lbShowRate.Text = "-";
                lbShowYear.Text = "-";
                labelDiscription.Text = "-";
                picBoxMain.Image = null;
                upStat();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to reset the Data Base.", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //public void searchName()
        //{
        //    toolStripProgressBar1.Maximum = listBox1.Items.Count;
        //    listBox1.Items.Clear();
        //    foreach (string name in Dic.recs.Keys)
        //    {
        //        if (name.ToLower().Contains(searchBox1.Text))
        //        {
        //            Game newGame = Dic.recs[name];
        //            labelDiscription.Text = newGame.Discription;
        //            lbShowName.Text = newGame.Name;
        //            lbShowGenre.Text = newGame.Genre;
        //            lbShowPlatform.Text = newGame.Platform;
        //            lbShowPublisher.Text = newGame.Publisher;
        //            lbShowRate.Text = newGame.Rate + "/5";
        //            lbShowYear.Text = newGame.Year;
        //            listBox1.Items.Add(newGame.Name);
        //            if (File.Exists("./imgs/image" + newGame.Id.ToString() + ".ai"))
        //            {
        //                Image img = new Bitmap("./imgs/image" + newGame.Id.ToString() + ".ai");
        //                picBoxMain.Image = img.GetThumbnailImage(210, 276, null, new IntPtr());
        //                img.Dispose();
        //            }
        //            else
        //            {
        //                picBoxMain.Image = null;
        //            }
        //        }
        //        toolStripProgressBar1.Increment(1);
        //    }
        //    toolStripProgressBar1.Value = 0;
        //}
        public static void setIndexFile()
        {
            if (!File.Exists("index.xml"))
            {
                new XDocument(
                            new XElement("root",
                                new XElement("index", Properties.Settings.Default.trackNums)
                                )).Save("index.xml");
            }
            else
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("index.xml");
                XmlNode rootNode = doc.SelectSingleNode("root");
                XmlNode targetNode = rootNode.SelectSingleNode("index");
                targetNode.InnerText = (Properties.Settings.Default.trackNums).ToString();
                doc.Save("index.xml");
            }
        }
        public static void getIndexFile()
        {
            if (File.Exists("index.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("index.xml");
                XmlNode rootNode = doc.SelectSingleNode("root");
                XmlNode targetNode = rootNode.SelectSingleNode("index");
                Properties.Settings.Default.trackNums = Convert.ToInt32(targetNode.InnerText);
            }
        }
        private void mainForm_Load(object sender, EventArgs e)
        {
            getIndexFile();
            if (!Directory.Exists("./imgs"))
            {
                Directory.CreateDirectory("./imgs");
            }
            if (!File.Exists("GDB.xml"))
            {
                Properties.Settings.Default.trackNums = 1;
                Properties.Settings.Default.Save();
                setIndexFile();
            }
            if (File.Exists("faveList.xml"))
            {
                listForm.loadData();
            }
            loadData();
            refreshListBox(0);
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string currentGame = listBox1.GetItemText(listBox1.SelectedItem);
                Game newGame = Dic.recs[currentGame];
                labelDiscription.Text = newGame.Discription;
                lbShowName.Text = newGame.Name;
                lbShowGenre.Text = newGame.Genre;
                toolTip1.SetToolTip(lbShowGenre, newGame.Genre);
                lbShowPlatform.Text = newGame.Platform;
                toolTip1.SetToolTip(lbShowPlatform, newGame.Platform);
                lbShowPublisher.Text = newGame.Publisher;
                toolTip1.SetToolTip(lbShowPublisher, newGame.Publisher);
                lbShowRate.Text = newGame.Rate + "/5";
                lbShowYear.Text = newGame.Year;
                int id = newGame.Id;
                upStat(newGame);
                if (File.Exists("./imgs/image" + newGame.Id.ToString() + ".ai"))
                {
                    Image img = new Bitmap("./imgs/image" + newGame.Id.ToString() + ".ai");
                    picBoxMain.Image = img.GetThumbnailImage(210, 276, null, new IntPtr());
                    img.Dispose();
                }
                else
                {
                    picBoxMain.Image = null;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addRec recAdd = new addRec();
            recAdd.ShowDialog();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addRec recAdd = new addRec();
            recAdd.ShowDialog();
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshListBox(0);
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox aboutMe = new AboutBox();
            aboutMe.Show();
        }
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetDataBase();
        }
        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            updateRec();
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteRec();
        }
        //private void changeIndex()
        //{
        //    var ans1 = MessageBox.Show("USE WITH CARE!\n\nThis option sets the active index which is used to read the dataBase." +
        //        "\nSo think twice before you change it."
        //        + "\n\n(Current active index is " + Properties.Settings.Default.trackNums.ToString() + ")",
        //        "CAUSION", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        //    if (ans1 == DialogResult.Cancel)
        //    {
        //        return;
        //    }
        //    string x = Interaction.InputBox("Enter the active index : \n\nThis number should be:" +
        //        "\n1. Integer.\n2. Positive.\n3. One digit more than the index of last record.");
        //    try
        //    {
        //        Properties.Settings.Default.trackNums = Convert.ToInt32(x);
        //        Properties.Settings.Default.Save();
        //        setIndexFile();
        //        MessageBox.Show("Index is set to " + Properties.Settings.Default.trackNums.ToString() + ". ", "Done"
        //            , MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    catch (Exception)
        //    {
        //        MessageBox.Show("Failed to set the Active Index. \nCheck your inputs and try again.", "Failed",
        //            MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}
        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SortList(false);
            OpenFileDialog browser = new OpenFileDialog();
            browser.Title = "Import Data Base";
            browser.InitialDirectory = @"c:\";
            browser.Filter = "XML Files (*.xml)|*.xml";
            browser.FilterIndex = 1;
            browser.RestoreDirectory = true;
            if (browser.ShowDialog() == DialogResult.OK)
            {
                string fPath = browser.FileName;
                if (File.Exists("GDB.xml"))
                {
                    File.Delete("GDB.xml");
                }
                File.Copy(fPath, @".\GDB.xml");
                loadData();
                refreshListBox(0);

                _instance.listBox1.SetSelected(_instance.listBox1.Items.Count - 1, true);
                string lastGameIndex = listBox1.GetItemText(_instance.listBox1.SelectedItem);
                Game lastGame = Dic.recs[lastGameIndex];
                int candidateIndex = lastGame.Id + 1;
                Properties.Settings.Default.trackNums = candidateIndex;
                Properties.Settings.Default.Save();
                setIndexFile();
            }
        }
        private async void searchBox1_TextChanged(object sender, EventArgs e)
        {
            listBox1.DataSource = null;
            var task = Task.Run(() =>
            {
                var resultList = new List<string>();
                foreach (string item in Dic.recs.Keys)
                {
                    if (item.ToLower().Contains(searchBox1.Text.ToLower()))
                    {
                        resultList.Add(item);
                    }
                    Invoke((Action)(() =>
                    {
                        toolStripProgressBar1.Increment(1);
                    }));
                }
                Invoke((Action)(() =>
                {
                    toolStripProgressBar1.Value = 0;
                }));
                return resultList;
            });
            listBox1.DataSource = await task;
        }

        private void exportAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                if (Directory.Exists(path + @"/Pictures"))
                {
                    Directory.Delete(path + @"/Pictures", true);
                }
                Directory.CreateDirectory(path + @"/Pictures");
                if (File.Exists(path + @"/log.txt"))
                {
                    File.Delete(path + @"/log.txt");
                }
                File.Create(path + @"/log.txt").Dispose();
                TextWriter pen = new StreamWriter(path + @"/log.txt");
                foreach (string item in Dic.recs.Keys)
                {
                    Game insGame = Dic.recs[item];
                    string name = item.Replace(':', '-');
                    try
                    {
                        File.Copy(@"imgs/image" + insGame.Id.ToString() + ".ai", path + @"/Pictures/" + name + ".png");
                    }
                    catch (Exception) { }
                    pen.WriteLine(insGame.Id.ToString() + ") " + name);
                }
                pen.WriteLine();
                pen.WriteLine("********Ali Reza M********");
                pen.WriteLine("******ar_m7@yahoo.com******");
                pen.Close();
                if (File.Exists(path + @"/Game Database.xml"))
                {
                    File.Delete(path + @"/Game Database.xml");
                }
                File.Copy("GDB.xml", path + @"/Game Database.xml");
                MessageBox.Show("Data successfully exported to your Desktop.", "Done",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Export Database.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void addToFaves()
        {
            string currentGame = listBox1.GetItemText(listBox1.SelectedItem);
            Game newGame = Dic.recs[currentGame];
            try
            {
                newList.recs.Add(currentGame, newGame);
                if (!File.Exists("faveList.xml"))
                {
                    new XDocument(
                                new XElement("root",
                                    new XElement("game" + newGame.Id.ToString(),
                                        new XElement("id", newGame.Id),
                                        new XElement("name", newGame.Name),
                                        new XElement("year", newGame.Year.ToString()),
                                        new XElement("genre", newGame.Genre)
                                    ))).Save("faveList.xml");
                }
                else
                {
                    XDocument doc = XDocument.Load("faveList.xml");
                    XElement gameRoot = new XElement("game" + newGame.Id.ToString());
                    gameRoot.Add(
                        new XElement("id", newGame.Id),
                        new XElement("name", newGame.Name),
                        new XElement("year", newGame.Year.ToString()),
                        new XElement("genre", newGame.Genre)
                    );
                    doc.Element("root").Add(gameRoot);
                    doc.Save("faveList.xml");
                }
                addedLabel.Visible = true;
                var t = new Timer();
                t.Interval = 1000; // it will Tick in 1 seconds
                t.Tick += (s, err) =>
                {
                    addedLabel.Hide();
                    t.Stop();
                };
                t.Start();
                upStat(newGame);
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Already added!", "Duplicated Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void remFromFaves()
        {
            try
            {
                Game delGame = new Game();
                delGame = Dic.recs[listBox1.GetItemText(listBox1.SelectedItem)];
                listForm.deleteRec(listBox1.GetItemText(listBox1.SelectedItem));
                upStat(delGame);
            }
            catch (Exception)
            {
                MessageBox.Show("Something is wrong.", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static void SortList(bool b)
        {
            refreshListBox(0);
            _instance.listBox1.Sorted = b;
            if (b)
            {
                Properties.Settings.Default.isSorted = true;
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.isSorted = false;
                Properties.Settings.Default.Save();
            }
        }
        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int location = listBox1.IndexFromPoint(e.Location);
                if (e.Button == MouseButtons.Right)
                {
                    listBox1.SelectedIndex = location;                //Index selected
                    if (newList.recs.ContainsKey(_instance.listBox1.GetItemText(_instance.listBox1.SelectedItem)))
                    {
                        addToFavoritesToolStripMenuItem.Visible = false;
                        removeFromFavesToolStripMenuItem.Visible = true;
                    }
                    else
                    {
                        addToFavoritesToolStripMenuItem.Visible = true;
                        removeFromFavesToolStripMenuItem.Visible = false;
                    }
                    contextMenuStrip1.Show(PointToScreen(e.Location));   //Show Menu
                }
            }
        }
        private void updateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            updateRec();
        }
        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            deleteRec();
        }
        private void addToFavoritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addToFaves();
        }
        private void btnShowFaves_Click(object sender, EventArgs e)
        {
            listForm favesForm = new listForm();
            favesForm.ShowDialog();
        }
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options options = new Options();
            options.ShowDialog();
        }

        private void btnHeart_Click(object sender, EventArgs e)
        {
            addToFaves();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists("GDB.xml"))
            {
                htmlMaker();
                if (!File.Exists("reportStyle.css"))
                {
                    string cssContent = "*{box-sizing:border-box;}.id{display:none;}" +
                        "body{text-align:center;color:white;margin:2%;background-color:black;}" +
                        "body>:nth-child(odd){margin:auto;background-color:brown;border:1px solid white;display:block;padding:1%;}" +
                        "body>:nth-child(even){margin:auto;background-color:#001A33;border:1px solid white;display:block;padding:1%;}" +
                        "p{display:block;text-align:left;margin-right:35 %;}.description{margin-right:35%;}.platform{margin-right:35%;}";
                    File.WriteAllText(@".\reportStyle.css", cssContent);
                }
                if (!File.Exists("script.js"))
                {
                    string jsContent = "var el = document.getElementsByTagName('div');" +
                        "var elId = document.getElementsByClassName('id');" +
                        "for (var i=1; i<=el.length; i++) {if(i % 2 != 0){" +
                        "el[i-1].style.background = \"url('imgs/image\" + elId[i-1].innerHTML + \".ai') no-repeat right , brown \";} else{" +
                        "el[i-1].style.background = \"url('imgs/image\" + elId[i-1].innerHTML + \".ai') no-repeat right , #001A33 \";}}";
                    File.WriteAllText(@".\script.js", jsContent);
                }
                Process.Start(Path.GetFullPath("htmlGDB.html"));
            }
            else
            {
                MessageBox.Show("No Database!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void removeFromFavesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            remFromFaves();
        }
    }
}
