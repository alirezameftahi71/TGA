﻿namespace GDB
{
    partial class listForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.picBoxList = new System.Windows.Forms.PictureBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupInfo = new System.Windows.Forms.GroupBox();
            this.lbGenre = new System.Windows.Forms.Label();
            this.lbYear = new System.Windows.Forms.Label();
            this.lbShowYear = new System.Windows.Forms.Label();
            this.lbShowGenre = new System.Windows.Forms.Label();
            this.lbShowName = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.btnClearAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxList)).BeginInit();
            this.groupInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(157, 225);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // picBoxList
            // 
            this.picBoxList.Location = new System.Drawing.Point(399, 13);
            this.picBoxList.Name = "picBoxList";
            this.picBoxList.Size = new System.Drawing.Size(113, 134);
            this.picBoxList.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxList.TabIndex = 14;
            this.picBoxList.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(401, 166);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(113, 28);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupInfo
            // 
            this.groupInfo.Controls.Add(this.lbGenre);
            this.groupInfo.Controls.Add(this.lbYear);
            this.groupInfo.Controls.Add(this.lbShowYear);
            this.groupInfo.Controls.Add(this.lbShowGenre);
            this.groupInfo.Controls.Add(this.lbShowName);
            this.groupInfo.Controls.Add(this.lbName);
            this.groupInfo.Location = new System.Drawing.Point(175, 13);
            this.groupInfo.Name = "groupInfo";
            this.groupInfo.Size = new System.Drawing.Size(218, 224);
            this.groupInfo.TabIndex = 12;
            this.groupInfo.TabStop = false;
            this.groupInfo.Text = "Info";
            // 
            // lbGenre
            // 
            this.lbGenre.AutoSize = true;
            this.lbGenre.Location = new System.Drawing.Point(6, 140);
            this.lbGenre.Name = "lbGenre";
            this.lbGenre.Size = new System.Drawing.Size(36, 13);
            this.lbGenre.TabIndex = 0;
            this.lbGenre.Text = "Genre";
            // 
            // lbYear
            // 
            this.lbYear.AutoSize = true;
            this.lbYear.Location = new System.Drawing.Point(6, 89);
            this.lbYear.Name = "lbYear";
            this.lbYear.Size = new System.Drawing.Size(71, 13);
            this.lbYear.TabIndex = 0;
            this.lbYear.Text = "Release Year";
            // 
            // lbShowYear
            // 
            this.lbShowYear.AutoSize = true;
            this.lbShowYear.Location = new System.Drawing.Point(90, 89);
            this.lbShowYear.Name = "lbShowYear";
            this.lbShowYear.Size = new System.Drawing.Size(10, 13);
            this.lbShowYear.TabIndex = 0;
            this.lbShowYear.Text = "-";
            // 
            // lbShowGenre
            // 
            this.lbShowGenre.Location = new System.Drawing.Point(90, 140);
            this.lbShowGenre.Name = "lbShowGenre";
            this.lbShowGenre.Size = new System.Drawing.Size(122, 55);
            this.lbShowGenre.TabIndex = 0;
            this.lbShowGenre.Text = "-";
            // 
            // lbShowName
            // 
            this.lbShowName.Location = new System.Drawing.Point(90, 30);
            this.lbShowName.Name = "lbShowName";
            this.lbShowName.Size = new System.Drawing.Size(122, 40);
            this.lbShowName.TabIndex = 0;
            this.lbShowName.Text = "-";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(6, 30);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(35, 13);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Name";
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(401, 200);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(113, 28);
            this.btnClearAll.TabIndex = 0;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // listForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 249);
            this.Controls.Add(this.btnClearAll);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.picBoxList);
            this.Controls.Add(this.groupInfo);
            this.Controls.Add(this.listBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "listForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Favorite List";
            this.Load += new System.EventHandler(this.listForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxList)).EndInit();
            this.groupInfo.ResumeLayout(false);
            this.groupInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox picBoxList;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupInfo;
        private System.Windows.Forms.Label lbGenre;
        private System.Windows.Forms.Label lbYear;
        private System.Windows.Forms.Label lbShowYear;
        private System.Windows.Forms.Label lbShowGenre;
        private System.Windows.Forms.Label lbShowName;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Button btnClearAll;
    }
}