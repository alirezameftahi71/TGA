﻿namespace GDB
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupDiscription = new System.Windows.Forms.GroupBox();
            this.labelDiscription = new System.Windows.Forms.Label();
            this.groupInfo = new System.Windows.Forms.GroupBox();
            this.lbGenre = new System.Windows.Forms.Label();
            this.lbYear = new System.Windows.Forms.Label();
            this.lbShowYear = new System.Windows.Forms.Label();
            this.lbShowRate = new System.Windows.Forms.Label();
            this.lbShowPublisher = new System.Windows.Forms.Label();
            this.lbShowPlatform = new System.Windows.Forms.Label();
            this.lbShowGenre = new System.Windows.Forms.Label();
            this.lbShowName = new System.Windows.Forms.Label();
            this.lbPublisher = new System.Windows.Forms.Label();
            this.lbPlatform = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lbRate = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.searchBox1 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.addedLabel = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.updateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToFavoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeFromFavesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowFaves = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnHeart = new System.Windows.Forms.Button();
            this.picBoxMain = new System.Windows.Forms.PictureBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupDiscription.SuspendLayout();
            this.groupInfo.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMain)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(16, 5, 0, 5);
            this.menuStrip1.Size = new System.Drawing.Size(2064, 55);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.toolStripSeparator,
            this.importToolStripMenuItem,
            this.exportAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(75, 45);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(315, 46);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("resetToolStripMenuItem.Image")));
            this.resetToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(315, 46);
            this.resetToolStripMenuItem.Text = "&RESET";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(312, 6);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importToolStripMenuItem.Image")));
            this.importToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.ShowShortcutKeys = false;
            this.importToolStripMenuItem.Size = new System.Drawing.Size(315, 46);
            this.importToolStripMenuItem.Text = "&Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportAsToolStripMenuItem
            // 
            this.exportAsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportAsToolStripMenuItem.Image")));
            this.exportAsToolStripMenuItem.Name = "exportAsToolStripMenuItem";
            this.exportAsToolStripMenuItem.ShowShortcutKeys = false;
            this.exportAsToolStripMenuItem.Size = new System.Drawing.Size(315, 46);
            this.exportAsToolStripMenuItem.Text = "Export";
            this.exportAsToolStripMenuItem.Click += new System.EventHandler(this.exportAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(312, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(315, 46);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(80, 45);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("updateToolStripMenuItem.Image")));
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.ShowShortcutKeys = false;
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(267, 46);
            this.updateToolStripMenuItem.Text = "&Update Rec";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripMenuItem.Image")));
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShowShortcutKeys = false;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(267, 46);
            this.deleteToolStripMenuItem.Text = "&Delete Rec";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(99, 45);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportToolStripMenuItem.Image")));
            this.reportToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.ShowShortcutKeys = false;
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(332, 46);
            this.reportToolStripMenuItem.Text = "Generate Report";
            this.reportToolStripMenuItem.Click += new System.EventHandler(this.reportToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("optionsToolStripMenuItem.Image")));
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(332, 46);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(92, 45);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Enabled = false;
            this.contentsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("contentsToolStripMenuItem.Image")));
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(235, 46);
            this.contentsToolStripMenuItem.Text = "&Help";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(232, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(235, 46);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // groupDiscription
            // 
            this.groupDiscription.Controls.Add(this.labelDiscription);
            this.groupDiscription.Location = new System.Drawing.Point(467, 64);
            this.groupDiscription.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupDiscription.Name = "groupDiscription";
            this.groupDiscription.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupDiscription.Size = new System.Drawing.Size(955, 322);
            this.groupDiscription.TabIndex = 2;
            this.groupDiscription.TabStop = false;
            this.groupDiscription.Text = "Discription";
            // 
            // labelDiscription
            // 
            this.labelDiscription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscription.Location = new System.Drawing.Point(8, 38);
            this.labelDiscription.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelDiscription.Name = "labelDiscription";
            this.labelDiscription.Size = new System.Drawing.Size(939, 277);
            this.labelDiscription.TabIndex = 0;
            this.labelDiscription.Text = "-";
            // 
            // groupInfo
            // 
            this.groupInfo.Controls.Add(this.lbGenre);
            this.groupInfo.Controls.Add(this.lbYear);
            this.groupInfo.Controls.Add(this.lbShowYear);
            this.groupInfo.Controls.Add(this.lbShowRate);
            this.groupInfo.Controls.Add(this.lbShowPublisher);
            this.groupInfo.Controls.Add(this.lbShowPlatform);
            this.groupInfo.Controls.Add(this.lbShowGenre);
            this.groupInfo.Controls.Add(this.lbShowName);
            this.groupInfo.Controls.Add(this.lbPublisher);
            this.groupInfo.Controls.Add(this.lbPlatform);
            this.groupInfo.Controls.Add(this.lbName);
            this.groupInfo.Controls.Add(this.lbRate);
            this.groupInfo.Location = new System.Drawing.Point(467, 401);
            this.groupInfo.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupInfo.Name = "groupInfo";
            this.groupInfo.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupInfo.Size = new System.Drawing.Size(955, 522);
            this.groupInfo.TabIndex = 3;
            this.groupInfo.TabStop = false;
            this.groupInfo.Text = "Info";
            // 
            // lbGenre
            // 
            this.lbGenre.AutoSize = true;
            this.lbGenre.Location = new System.Drawing.Point(16, 215);
            this.lbGenre.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbGenre.Name = "lbGenre";
            this.lbGenre.Size = new System.Drawing.Size(94, 32);
            this.lbGenre.TabIndex = 0;
            this.lbGenre.Text = "Genre";
            // 
            // lbYear
            // 
            this.lbYear.AutoSize = true;
            this.lbYear.Location = new System.Drawing.Point(16, 143);
            this.lbYear.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbYear.Name = "lbYear";
            this.lbYear.Size = new System.Drawing.Size(187, 32);
            this.lbYear.TabIndex = 0;
            this.lbYear.Text = "Release Year";
            // 
            // lbShowYear
            // 
            this.lbShowYear.AutoSize = true;
            this.lbShowYear.Location = new System.Drawing.Point(240, 143);
            this.lbShowYear.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowYear.Name = "lbShowYear";
            this.lbShowYear.Size = new System.Drawing.Size(24, 32);
            this.lbShowYear.TabIndex = 0;
            this.lbShowYear.Text = "-";
            // 
            // lbShowRate
            // 
            this.lbShowRate.AutoSize = true;
            this.lbShowRate.Location = new System.Drawing.Point(240, 427);
            this.lbShowRate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowRate.Name = "lbShowRate";
            this.lbShowRate.Size = new System.Drawing.Size(24, 32);
            this.lbShowRate.TabIndex = 0;
            this.lbShowRate.Text = "-";
            // 
            // lbShowPublisher
            // 
            this.lbShowPublisher.AutoSize = true;
            this.lbShowPublisher.Location = new System.Drawing.Point(240, 358);
            this.lbShowPublisher.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowPublisher.Name = "lbShowPublisher";
            this.lbShowPublisher.Size = new System.Drawing.Size(24, 32);
            this.lbShowPublisher.TabIndex = 0;
            this.lbShowPublisher.Text = "-";
            // 
            // lbShowPlatform
            // 
            this.lbShowPlatform.AutoEllipsis = true;
            this.lbShowPlatform.AutoSize = true;
            this.lbShowPlatform.Location = new System.Drawing.Point(240, 286);
            this.lbShowPlatform.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowPlatform.MaximumSize = new System.Drawing.Size(693, 72);
            this.lbShowPlatform.Name = "lbShowPlatform";
            this.lbShowPlatform.Size = new System.Drawing.Size(19, 37);
            this.lbShowPlatform.TabIndex = 0;
            this.lbShowPlatform.Text = "-";
            this.lbShowPlatform.UseCompatibleTextRendering = true;
            // 
            // lbShowGenre
            // 
            this.lbShowGenre.AutoSize = true;
            this.lbShowGenre.Location = new System.Drawing.Point(240, 215);
            this.lbShowGenre.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowGenre.Name = "lbShowGenre";
            this.lbShowGenre.Size = new System.Drawing.Size(24, 32);
            this.lbShowGenre.TabIndex = 0;
            this.lbShowGenre.Text = "-";
            // 
            // lbShowName
            // 
            this.lbShowName.AutoSize = true;
            this.lbShowName.Location = new System.Drawing.Point(240, 72);
            this.lbShowName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbShowName.Name = "lbShowName";
            this.lbShowName.Size = new System.Drawing.Size(24, 32);
            this.lbShowName.TabIndex = 0;
            this.lbShowName.Text = "-";
            // 
            // lbPublisher
            // 
            this.lbPublisher.AutoSize = true;
            this.lbPublisher.Location = new System.Drawing.Point(16, 358);
            this.lbPublisher.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbPublisher.Name = "lbPublisher";
            this.lbPublisher.Size = new System.Drawing.Size(135, 32);
            this.lbPublisher.TabIndex = 0;
            this.lbPublisher.Text = "Publisher";
            // 
            // lbPlatform
            // 
            this.lbPlatform.AutoSize = true;
            this.lbPlatform.Location = new System.Drawing.Point(16, 286);
            this.lbPlatform.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbPlatform.Name = "lbPlatform";
            this.lbPlatform.Size = new System.Drawing.Size(121, 32);
            this.lbPlatform.TabIndex = 0;
            this.lbPlatform.Text = "Platform";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(16, 72);
            this.lbName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(90, 32);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Name";
            // 
            // lbRate
            // 
            this.lbRate.AutoSize = true;
            this.lbRate.Location = new System.Drawing.Point(16, 427);
            this.lbRate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbRate.Name = "lbRate";
            this.lbRate.Size = new System.Drawing.Size(75, 32);
            this.lbRate.TabIndex = 0;
            this.lbRate.Text = "Rate";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 31;
            this.listBox1.Location = new System.Drawing.Point(32, 200);
            this.listBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(412, 717);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseUp);
            // 
            // searchBox1
            // 
            this.searchBox1.AcceptsReturn = true;
            this.searchBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.searchBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.searchBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.searchBox1.Location = new System.Drawing.Point(32, 138);
            this.searchBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.searchBox1.Name = "searchBox1";
            this.searchBox1.ShortcutsEnabled = false;
            this.searchBox1.Size = new System.Drawing.Size(412, 38);
            this.searchBox1.TabIndex = 11;
            this.searchBox1.WordWrap = false;
            this.searchBox1.TextChanged += new System.EventHandler(this.searchBox1_TextChanged);
            // 
            // addedLabel
            // 
            this.addedLabel.AutoSize = true;
            this.addedLabel.BackColor = System.Drawing.Color.Transparent;
            this.addedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addedLabel.ForeColor = System.Drawing.Color.Lime;
            this.addedLabel.Location = new System.Drawing.Point(1624, 756);
            this.addedLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.addedLabel.Name = "addedLabel";
            this.addedLabel.Size = new System.Drawing.Size(233, 58);
            this.addedLabel.TabIndex = 13;
            this.addedLabel.Text = "ADDED! ";
            this.addedLabel.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateToolStripMenuItem1,
            this.deleteToolStripMenuItem1,
            this.addToFavoritesToolStripMenuItem,
            this.removeFromFavesToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(353, 188);
            // 
            // updateToolStripMenuItem1
            // 
            this.updateToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("updateToolStripMenuItem1.Image")));
            this.updateToolStripMenuItem1.Name = "updateToolStripMenuItem1";
            this.updateToolStripMenuItem1.Size = new System.Drawing.Size(352, 46);
            this.updateToolStripMenuItem1.Text = "Update";
            this.updateToolStripMenuItem1.Click += new System.EventHandler(this.updateToolStripMenuItem1_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripMenuItem1.Image")));
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(352, 46);
            this.deleteToolStripMenuItem1.Text = "Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // addToFavoritesToolStripMenuItem
            // 
            this.addToFavoritesToolStripMenuItem.Image = global::GDB.Properties.Resources.hearts;
            this.addToFavoritesToolStripMenuItem.Name = "addToFavoritesToolStripMenuItem";
            this.addToFavoritesToolStripMenuItem.Size = new System.Drawing.Size(352, 46);
            this.addToFavoritesToolStripMenuItem.Text = "Add to Favorites";
            this.addToFavoritesToolStripMenuItem.Visible = false;
            this.addToFavoritesToolStripMenuItem.Click += new System.EventHandler(this.addToFavoritesToolStripMenuItem_Click);
            // 
            // removeFromFavesToolStripMenuItem
            // 
            this.removeFromFavesToolStripMenuItem.Image = global::GDB.Properties.Resources.remFavIcon;
            this.removeFromFavesToolStripMenuItem.Name = "removeFromFavesToolStripMenuItem";
            this.removeFromFavesToolStripMenuItem.Size = new System.Drawing.Size(352, 46);
            this.removeFromFavesToolStripMenuItem.Text = "Remove Favorites";
            this.removeFromFavesToolStripMenuItem.Visible = false;
            this.removeFromFavesToolStripMenuItem.Click += new System.EventHandler(this.removeFromFavesToolStripMenuItem_Click);
            // 
            // btnShowFaves
            // 
            this.btnShowFaves.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShowFaves.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShowFaves.Location = new System.Drawing.Point(1624, 827);
            this.btnShowFaves.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnShowFaves.Name = "btnShowFaves";
            this.btnShowFaves.Size = new System.Drawing.Size(264, 76);
            this.btnShowFaves.TabIndex = 0;
            this.btnShowFaves.Text = "Show Faves";
            this.btnShowFaves.UseVisualStyleBackColor = true;
            this.btnShowFaves.Click += new System.EventHandler(this.btnShowFaves_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 952);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(3, 0, 37, 0);
            this.statusStrip1.Size = new System.Drawing.Size(2064, 52);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(211, 47);
            this.toolStripStatusLabel1.Text = "Total records : ";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(267, 46);
            this.toolStripProgressBar1.Step = 1;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(246, 47);
            this.toolStripStatusLabel3.Text = "No item Selected";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 47);
            this.toolStripStatusLabel2.Text = "-";
            // 
            // btnHeart
            // 
            this.btnHeart.BackColor = System.Drawing.SystemColors.Control;
            this.btnHeart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHeart.BackgroundImage")));
            this.btnHeart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnHeart.FlatAppearance.BorderSize = 0;
            this.btnHeart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHeart.Location = new System.Drawing.Point(331, 76);
            this.btnHeart.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnHeart.Name = "btnHeart";
            this.btnHeart.Size = new System.Drawing.Size(96, 45);
            this.btnHeart.TabIndex = 15;
            this.btnHeart.UseVisualStyleBackColor = false;
            this.btnHeart.Click += new System.EventHandler(this.btnHeart_Click);
            // 
            // picBoxMain
            // 
            this.picBoxMain.Location = new System.Drawing.Point(1469, 131);
            this.picBoxMain.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.picBoxMain.Name = "picBoxMain";
            this.picBoxMain.Size = new System.Drawing.Size(560, 658);
            this.picBoxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxMain.TabIndex = 10;
            this.picBoxMain.TabStop = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.Control;
            this.btnRefresh.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh.BackgroundImage")));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(187, 72);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(96, 50);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(32, 76);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(96, 48);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(2064, 1004);
            this.Controls.Add(this.btnHeart);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnShowFaves);
            this.Controls.Add(this.addedLabel);
            this.Controls.Add(this.searchBox1);
            this.Controls.Add(this.picBoxMain);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.groupInfo);
            this.Controls.Add(this.groupDiscription);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.Name = "mainForm";
            this.Text = "Game Archive";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupDiscription.ResumeLayout(false);
            this.groupInfo.ResumeLayout(false);
            this.groupInfo.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupDiscription;
        private System.Windows.Forms.GroupBox groupInfo;
        private System.Windows.Forms.Label labelDiscription;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label lbGenre;
        private System.Windows.Forms.Label lbYear;
        private System.Windows.Forms.Label lbPublisher;
        private System.Windows.Forms.Label lbPlatform;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbRate;
        private System.Windows.Forms.Label lbShowYear;
        private System.Windows.Forms.Label lbShowRate;
        private System.Windows.Forms.Label lbShowPublisher;
        private System.Windows.Forms.Label lbShowPlatform;
        private System.Windows.Forms.Label lbShowGenre;
        private System.Windows.Forms.Label lbShowName;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.PictureBox picBoxMain;
        private System.Windows.Forms.TextBox searchBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label addedLabel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addToFavoritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.Button btnShowFaves;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnHeart;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem removeFromFavesToolStripMenuItem;
    }
}

