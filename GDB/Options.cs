﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GDB
{
    public partial class Options : Form
    {
        private static Options _instance;
        public Options()
        {
            InitializeComponent();
            _instance = this;
            if (Properties.Settings.Default.isSorted)
            {
                sortChBox.Checked = true;
            }
            if (Properties.Settings.Default.isStatusEnabled)
            {
                statChBox.Checked = true;
            }
        }
        private void sortChBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sortChBox.Checked)
            {
                mainForm.SortList(true);
            }
            else
            {
                mainForm.SortList(false);
                mainForm.refreshListBox();
            }
        }
        private void statChBox_CheckedChanged(object sender, EventArgs e)
        {
            if (statChBox.Checked)
            {
                Properties.Settings.Default.isStatusEnabled = true;
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.isStatusEnabled = false;
                Properties.Settings.Default.Save();
            }
            mainForm.upStat();
        }
    }
}
