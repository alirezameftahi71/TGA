﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

/* 
 * Alireza Meftahi
*/

namespace GDB
{
    public partial class listForm : Form
    {
        private static listForm _instance2;

        public listForm()
        {
            InitializeComponent();
            _instance2 = this;
        }
        public static void loadData()
        {
            try
            {
                if (File.Exists("faveList.xml"))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load("faveList.xml");
                    XmlNode rootNode = doc.SelectSingleNode("root");
                    foreach (XmlNode gameNode in rootNode.ChildNodes)
                    {
                        Game loadGame = new Game();
                        XmlNode idNode = gameNode.SelectSingleNode("id");
                        XmlNode nameNode = gameNode.SelectSingleNode("name");
                        XmlNode yearNode = gameNode.SelectSingleNode("year");
                        XmlNode genreNode = gameNode.SelectSingleNode("genre");
                        loadGame.Id = Convert.ToInt32(idNode.InnerText);
                        loadGame.Name = nameNode.InnerText;
                        loadGame.Year = yearNode.InnerText;
                        loadGame.Genre = genreNode.InnerText;
                        newList.recs.Add(loadGame.Name, loadGame);
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void refreshListBox(int x)
        {
            listBox1.Items.Clear();
            lbShowName.Text = "-";
            lbShowYear.Text = "-";
            lbShowGenre.Text = "-";
            picBoxList.Image = null;
            try
            {
                foreach (string candidateG in newList.recs.Keys)
                {
                    listBox1.Items.Add(candidateG);
                }
                listBox1.SelectedIndex = x;
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void deleteRec()
        {
            if (File.Exists("faveList.xml"))
            {
                try
                {
                    var i = MessageBox.Show("Are you sure you want to remove this record from the list?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (i.ToString() == "No")
                    {
                        return;
                    }
                    int x = listBox1.SelectedIndex;
                    Game delGame = new Game();
                    delGame = newList.recs[listBox1.GetItemText(listBox1.SelectedItem)];
                    XmlDocument doc = new XmlDocument();
                    doc.Load("faveList.xml");
                    XmlNode rootNode = doc.SelectSingleNode("root");
                    XmlNode gameNode = rootNode.SelectSingleNode("game" + delGame.Id.ToString());
                    rootNode.RemoveChild(gameNode);
                    doc.Save("faveList.xml");
                    newList.recs.Remove(listBox1.GetItemText(listBox1.SelectedItem));
                    picBoxList.Image = null;
                    refreshListBox(x > 0 ? (x - 1) : (0));
                    mainForm.upStat(delGame);
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to Remove", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No Data Base!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static void deleteRec(string name)
        {
            if (File.Exists("faveList.xml"))
            {
                try
                {
                    Game delGame = new Game();
                    delGame = newList.recs[name];
                    XmlDocument doc = new XmlDocument();
                    doc.Load("faveList.xml");
                    XmlNode rootNode = doc.SelectSingleNode("root");
                    XmlNode gameNode = rootNode.SelectSingleNode("game" + delGame.Id.ToString());
                    rootNode.RemoveChild(gameNode);
                    doc.Save("faveList.xml");
                    newList.recs.Remove(name);
                    _instance2.picBoxList.Image = null;
                }
                catch (Exception) { /* Do nothing */ }
            }
        }

        private void listForm_Load(object sender, EventArgs e)
        {
            loadData();
            refreshListBox(0);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string currentGame = listBox1.GetItemText(listBox1.SelectedItem);
                Game newGame = newList.recs[currentGame];
                lbShowName.Text = newGame.Name;
                lbShowGenre.Text = newGame.Genre;
                lbShowYear.Text = newGame.Year;
                int id = newGame.Id;
                if (File.Exists("./imgs/image" + id.ToString() + ".ai"))
                {
                    Image img = new Bitmap("./imgs/image" + id.ToString() + ".ai");
                    picBoxList.Image = img.GetThumbnailImage(113, 134, null, new IntPtr());
                    img.Dispose();
                }
                else
                {
                    picBoxList.Image = null;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(err.Message, "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteRec();
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            if (File.Exists("faveList.xml"))
            {
                try
                {
                    var i = MessageBox.Show("Are you sure you want to remove all the records?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (i.ToString() == "No")
                    {
                        return;
                    }
                    File.Delete("faveList.xml");
                    listBox1.Items.Clear();
                    foreach (var item in newList.recs)
                    {
                        newList.recsdel.Add(item.Key, item.Value);
                    }
                    newList.recs.Clear();
                    newList.candidateGame = null;
                    foreach (Game delCandidate in newList.recsdel.Values)
                    {
                        mainForm.upStat(delCandidate);
                    }
                    picBoxList.Image = null;
                    lbShowName.Text = "-";
                    lbShowGenre.Text = "-";
                    lbShowYear.Text = "-";
                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to Clear", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No Data Base!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
